
data "aws_caller_identity" "current" {}


provider "aws" {
    access_key = ""
    secret_key = ""    
    region = "eu-west-2"  
}


# Client Cognito
module "module_create_cognito_client" {
    source            = "./Cognito"
    name     = "cutitronics-development-client-pool"
    username_attributes = [ "email"]
    allow_admin_create_user_only = false
    email_subject = "Your verification code"
    email_message = "Your username is {username} and your temporary password is '{####}'."
    sms_message = "{username} verification code is {####}."
    auto_verified_attributes = ["email"]
    challenge_required_on_new_device = true
    device_only_remembered_on_user_prompt = false
    enable_username_case_sensitivity = true
    minimum_length    = 10
    require_lowercase = true
    require_numbers   = true
    require_uppercase = true
    require_symbols   = true
    temporary_password_validity_days = 1
    allowed_oauth_flows = ["implicit"]
    allowed_oauth_flows_user_pool_client = true
    generate_secret  = false
    explicit_auth_flows = ["ALLOW_ADMIN_USER_PASSWORD_AUTH", "ALLOW_REFRESH_TOKEN_AUTH", "ALLOW_USER_PASSWORD_AUTH"]
    prevent_user_existence_errors = "ENABLED"
    refresh_token_validity = 30
}


# Therapist Cognito
module "module_create_cognito_therapist" {
    source            = "./Cognito"
    name     = "cutitronics-development-therapist-pool"
    username_attributes = [ "email"]
    allow_admin_create_user_only = false
    email_subject = "Your verification code"
    email_message = "Your username is {username} and your temporary password is '{####}'."
    sms_message = "{username} verification code is {####}."
    auto_verified_attributes = ["email"]
    challenge_required_on_new_device = true
    device_only_remembered_on_user_prompt = false
    enable_username_case_sensitivity = true
    minimum_length    = 10
    require_lowercase = true
    require_numbers   = true
    require_uppercase = true
    require_symbols   = true
    temporary_password_validity_days = 1
    allowed_oauth_flows = ["implicit"]
    allowed_oauth_flows_user_pool_client = true
    generate_secret  = false
    explicit_auth_flows = ["ALLOW_ADMIN_USER_PASSWORD_AUTH", "ALLOW_REFRESH_TOKEN_AUTH", "ALLOW_USER_PASSWORD_AUTH"]
    prevent_user_existence_errors = "ENABLED"
    refresh_token_validity = 30

}


# System Cognito
module "module_create_cognito_system" {
    source            = "./Cognito"
    name     = "cutitronics-development-system-pool"
    username_attributes = [ "email"]
    allow_admin_create_user_only = false
    email_subject = "Your verification code"
    email_message = "Your username is {username} and your temporary password is '{####}'."
    sms_message = "{username} verification code is {####}."
    auto_verified_attributes = ["email"]
    challenge_required_on_new_device = true
    device_only_remembered_on_user_prompt = false
    enable_username_case_sensitivity = true
    minimum_length    = 10
    require_lowercase = true
    require_numbers   = true
    require_uppercase = true
    require_symbols   = true
    temporary_password_validity_days = 1
    allowed_oauth_flows = ["implicit"]
    allowed_oauth_flows_user_pool_client = true
    generate_secret  = false
    explicit_auth_flows = ["ALLOW_ADMIN_USER_PASSWORD_AUTH", "ALLOW_REFRESH_TOKEN_AUTH", "ALLOW_USER_PASSWORD_AUTH"]
    prevent_user_existence_errors = "ENABLED"
    refresh_token_validity = 30
}


# Admin Cognito
module "module_create_cognito_admin" {
    source            = "./Cognito"
    name     = "cutitronics-development-admin-pool"
    username_attributes = [ "email"]
    allow_admin_create_user_only = false
    email_subject = "Your verification code"
    email_message = "Your username is {username} and your temporary password is '{####}'."
    sms_message = "{username} verification code is {####}."
    auto_verified_attributes = ["email"]
    challenge_required_on_new_device = true
    device_only_remembered_on_user_prompt = false
    enable_username_case_sensitivity = true
    minimum_length    = 10
    require_lowercase = true
    require_numbers   = true
    require_uppercase = true
    require_symbols   = true
    temporary_password_validity_days = 1
    allowed_oauth_flows = ["implicit"]
    allowed_oauth_flows_user_pool_client = true
    generate_secret  = false
    explicit_auth_flows = ["ALLOW_ADMIN_USER_PASSWORD_AUTH", "ALLOW_REFRESH_TOKEN_AUTH", "ALLOW_USER_PASSWORD_AUTH"]
    prevent_user_existence_errors = "ENABLED"
    refresh_token_validity = 30
}
