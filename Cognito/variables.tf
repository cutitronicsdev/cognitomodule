
variable "name" {
  type        = string
  description = "(Required) The name of the user pool."
}


variable "username_attributes" {
  type        = list(string)
  description = "(Optional) Specifies whether email addresses or phone numbers can be specified as usernames when a user signs up. Conflicts with alias_attributes"
  default     = null
}


variable "allow_admin_create_user_only"{
  type = bool
  description = "(Optional) Set to True if only the administrator is allowed to create user profiles. Set to False if users can sign themselves up via an app. Default is true."
  default = false
}


variable "email_subject" {
  type        = string
  description = "(Opitonal) The message template for email messages. Default is Your verification code."
  default = "Your verification code"
}


variable "email_message" {
  type        = string
  description = "(Opitonal) The message template for email messages. Must contain {username} and {####} placeholders, for username and temporary password, respectively. Default is Your username is {username} and your temporary password is '{####}'"
  default = "Your verification code"
}


variable "sms_message" {
  type        = string
  description = "(Opitonal) The message template for email messages. Must contain {username} and {####} placeholders, for username and temporary password, respectively. Default is Your username is {username} and your temporary password is '{####}'"
  default = "Your verification code"
}


variable "auto_verified_attributes" {
  type        = set(string)
  description = "(Optional) The attributes to be auto-verified. Possible values: 'email', 'phone_number'."
  default = [
    "email"
  ]
}


variable "challenge_required_on_new_device" {
  type        = bool
  description = "(Optional) Indicates whether a challenge is required on a new device. Only applicable to a new device."
  default     = true
}


variable "device_only_remembered_on_user_prompt" {
  type        = bool
  description = "(Optional) If true, a device is only remembered on user prompt."
  default     = true
}


variable "enable_username_case_sensitivity" {
  type        = bool
  description = "(Optional) Specifies whether username case sensitivity will be applied for all users in the user pool through Cognito APIs."
  default     = false
}


# Password Policy


variable "minimum_length" {
  type        = number
  description = "(Optional) - The minimum length of the password policy that you have set. Default is 20."
  default     = 10
}


variable "require_uppercase" {
  type        = bool
  description = "(Optional) - Whether you have required users to use at least one uppercase letter in their password."
  default     = true
}


variable "require_lowercase" {
  type        = bool
  description = "(Optional) - Whether you have required users to use at least one lowercase letter in their password."
  default     = true
}


variable "require_numbers" {
  type        = bool
  description = "(Optional) - Whether you have required users to use at least one number in their password."
  default     = true
}


variable "require_symbols" {
  type        = bool
  description = "(Optional) - Whether you have required users to use at least one symbol in their password."
  default     = true
}


variable "temporary_password_validity_days" {
  type        = number
  description = "(Optional) - In the password policy you have set, refers to the number of days a temporary password is valid. If the user does not sign-in during this time, their password will need to be reset by an administrator."
  default     = 1
}


## User pool


variable "allowed_oauth_flows" {
  type        = list(string)
  description = "(Optional) List of allowed OAuth flows (code, implicit, client_credentials)."
  default     = null
}


variable "allowed_oauth_flows_user_pool_client" {
  type        = bool
  description = "(Optional) Whether the client is allowed to follow the OAuth protocol when interacting with Cognito user pools."
  default     = null
}


variable "generate_secret" {
  type        = bool
  description = "(Optional) Should an application secret be generated."
  default     = true
}


variable "explicit_auth_flows" {
  description = "(Optional) List of authentication flows. Possible values are 'ADMIN_NO_SRP_AUTH', 'CUSTOM_AUTH_FLOW_ONLY', 'USER_PASSWORD_AUTH', 'ALLOW_ADMIN_USER_PASSWORD_AUTH', 'ALLOW_CUSTOM_AUTH', 'ALLOW_USER_PASSWORD_AUTH', 'ALLOW_USER_SRP_AUTH', and 'ALLOW_REFRESH_TOKEN_AUTH'."
  type        = list(string)
  default     = null
}


variable "prevent_user_existence_errors" {
  description = "(Optional) Choose which errors and responses are returned by Cognito APIs during authentication, account confirmation, and password recovery when the user does not exist in the Cognito User Pool. When set to 'ENABLED' and the user does not exist, authentication returns an error indicating either the username or password was incorrect, and account confirmation and password recovery return a response indicating a code was sent to a simulated destination. When set to 'LEGACY', those APIs will return a 'UserNotFoundException' exception if the user does not exist in the Cognito User Pool."
  type        = string
  default     = null
}


variable "refresh_token_validity" {
  description = "(Optional) The time limit in days refresh tokens are valid for."
  type        = number
  default     = 30
}