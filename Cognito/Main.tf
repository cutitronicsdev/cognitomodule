

resource "aws_cognito_user_pool" "Cutitronics-pool" {
  
  name                          = var.name
  username_attributes           = var.username_attributes
  admin_create_user_config {
    allow_admin_create_user_only = var.allow_admin_create_user_only
    # invite_message_template {
    #   email_subject = "${var.email_subject}"
    #   email_message = "${var.email_message}"
    #   sms_message   = "${var.sms_message}"
    # }
    
  }

  auto_verified_attributes = var.auto_verified_attributes
  device_configuration {
      challenge_required_on_new_device      = var.challenge_required_on_new_device
      device_only_remembered_on_user_prompt = var.device_only_remembered_on_user_prompt
    }
  username_configuration {
    case_sensitive = var.enable_username_case_sensitivity
  }
  password_policy {
    minimum_length                   = var.minimum_length
    require_uppercase                = var.require_uppercase
    require_lowercase                = var.require_lowercase
    require_numbers                  = var.require_numbers
    require_symbols                  = var.require_symbols
    temporary_password_validity_days = var.temporary_password_validity_days
  }

}



resource "aws_cognito_user_pool_client" "client" {
  
  user_pool_id = aws_cognito_user_pool.Cutitronics-pool.id
  name = "${var.name}"
  allowed_oauth_flows = var.allowed_oauth_flows
  allowed_oauth_flows_user_pool_client = var.allowed_oauth_flows_user_pool_client
  generate_secret = var.generate_secret
  explicit_auth_flows  = var.explicit_auth_flows
  prevent_user_existence_errors = var.prevent_user_existence_errors
  refresh_token_validity = var.refresh_token_validity
  callback_urls = ["https://example.com/callback"]
  allowed_oauth_scopes = ["openid", "email", "phone", "aws.cognito.signin.user.admin"]
  supported_identity_providers = ["COGNITO"]

}


resource "aws_cognito_user_pool_domain" "main" {

  domain          = var.name
  user_pool_id    = aws_cognito_user_pool.Cutitronics-pool.id

}


resource "aws_cognito_resource_server" "resource" {
  identifier = "cutitronics-client-api"
  name       = "cutitronics-client-api"
  scope {
    scope_name        = "client-scope"
    scope_description = "client-scope"
  }
  user_pool_id = aws_cognito_user_pool.Cutitronics-pool.id
}